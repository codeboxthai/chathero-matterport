module.exports = {
  apps: [
    {
      name: "chathero-matterport",
      script: "./start.js",
      env: {
        HOST: "localhost",
        PORT: 3001
      }
    }
  ]
};
